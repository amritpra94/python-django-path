## Setup

### PostgreSQL

**1. Install PostgreSQL**

You can find instructions online

**2. Create a schema called `bootcamp`**

```
CREATE DATABASE bootcamp;
```

**3. Create a schema to run your tests**

```
CREATE DATABASE bootcamp_test;
```

**3. Grant all privileges on `bootcamp` to yourself**


```
GRANT ALL PRIVILEGES ON DATABASE "bootcamp" to your_username;
GRANT ALL PRIVILEGES ON DATABASE "bootcamp_test" to your_username;
```
________

### Redis

**1. Install and run redis**

You can find instructions online

(Redis is used by django-channels for real-time communication)
________________

### Clone bootcamp:

https://github.com/vitorfs/bootcamp

`cd` to `bootcamp`
_____________

### Virtualenv, pip install:

**1. Create a `virtualenv`** 

Use `virtualenvwrapper` and set Python version to `3.6`

```
mkvirtualenv bootcamp --python=`which python3.6`
```

**2. Install requirements**

```
pip install -U pip
pip install -r requirements/base.txt
pip install -r requirements/local.txt
```

_________________

### Django setup


**1. settings**

The default settings provided by `bootcamp` on GitHub have been changed. You can use the settings provided in the `bootcamp_config` directory instead.

Change:

* `config/settings/base.py`

* `config/settings/local.py`

**Important**: Change the `DATABASES` settings according to your credentials

**2. Create `.env` file**

At the project root directory, create an empty file called `.env`

```
touch .env
```

**3. Django sanity check**

```
python manage.py check
```

You must NOT get any errors. You must see:

```
System check identified no issues (0 silenced).

```


**4. Create tables**

```
python manage.py migrate
```

**5. Start the server**

