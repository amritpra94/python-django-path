## Logging

* https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/

* What are log files?
* Why are they needed?
* Why you should never use print and use a logger instead?
* Various logging levels
* How to locate log files for various tools?
* log rotation
* syslog


* Centralized Logging tools
- https://www.elastic.co/products/logstash
- https://www.loggly.com/

* Monitoring / Observability - metrics like CPU, memory, disk etc across your infra

__________________________


## Demo - Debugging Nginx configuration

* Change configuration - nginx -t
* Get logs within particular time range to answer questions like "Why did the server have high response times between 8 AM and 9 AM yesterday?"

## systemd logging example

* https://stackoverflow.com/questions/37585758/how-to-redirect-output-of-systemd-service-to-a-file
