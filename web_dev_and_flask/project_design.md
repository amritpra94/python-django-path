### Define your user Stories

* https://en.wikipedia.org/wiki/User_story
* https://agileforgrowth.com/blog/how-to-write-good-user-stories/


### Define your MVP

* https://en.wikipedia.org/wiki/Minimum_viable_product

_______________________


### Build simple, low-fidelity wireframes

For each page, visualize

* What information is shown
* Hyperlinks to other pages
* What actions can be taken (traditional forms, ajax actions)

________

### Design your URLs

* Observe how sites like StackOverflow and GitHub have structured their URLs

Here's an article on how [GitHub designed their URLs](http://web.archive.org/web/20120408175647/https://warpspire.com/posts/url-design/)

#### Example:

If you're building a StackOverflow clone, you'll have the following URLs

**`/questions`** - Question List page

- The title of each question, its tags, the asker and number of upvotes / downvotes are shown. Each question has a hyperlink to the question detail page

**`/questions/<question_id>`** - Question Detail page.

- All the information regarding the question is shown
- Also, users can upvote / downvote the question
- A logged in user can add an answer 

**`/questions/new`** - New Question Page.

- Logged in users can create questions
- After the form is submitted and processed, the user will be redirected to the Question Detail page.

**`/questions/<question_id>/edit`** - Question Update Page

- The asker of a question can update their question
- After the form is submitted and processed, the user will be redirected to the Question Detail page.

Of course, you'll have other URLs too.
__________

### Define your forms

For each action, figure out what information needs to be entered by the user, and to which URL the user will be redirected after successfully saving the form.

For example, if a user needs to create a question on a site like StackOverflow, you'll have the following parameters

```json
{
    "title": "How do I trim whitespace from a Python string?",
    "description": "...",
    "tags": ["python", "string"]
}
```

__________

### Authentication / Authorization

* Define what permissions are required for each action. For example
    - Any user can view a question
    - Only the asker of a question can edit that question
    - Only logged in users can upvote/downvote a question

And show appropriate error messages when the user does not have permissions to perform an action



__________

### Database schema design

* Now that you're aware of all the features to be built in detail, design your models. 
* Follow database normalization guidelines. (See the `db` directory for more info)

________

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">One last talk idea: CRUD + Auth All The Way Down. Literally just go through FB/Instagram/Pinterest/etc and explain how they&#39;re all the same if you include foreign keys.<br><br>Not really an insight to people who follow me on Twitter, but to a beginner? It really is.</p>&mdash; William Vincent (@wsv3000) <a href="https://twitter.com/wsv3000/status/1126845305008402433?ref_src=twsrc%5Etfw">May 10, 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

_________


Projects: 

* SoundCloud
* PG Hunt
* Events
* Expense
* Blood donation
* Chat (Simple Slack clone)
* Stack Overflow
* YouTube
* Hacker News
* Library
