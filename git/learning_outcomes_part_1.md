## Learning Outcomes

### Understanding
You must have understood the following:

* What is version control?
* Why is version control used?
* What is git?
* At a high level, how does it work?
* What does the staging area mean?
* What is a branch?
* What is a remote?
* Undertand `git status`


___________________

### Operations

You must be able to execute the following git operations on the command line.


* Initialize a repository
* Add files to the staging area
* Commit files with proper messages
* Push a branch to GitLab
* Clone a repo
* View all commits


### Drill

You need to execute a bunch of git commands. Save all your commands in a text file called `git_drill_part_1.txt`

1. Create a directory called `git_sample_project` and `cd` to `git_sample_project`
2. Initialize a git repo.
3. Create the following files `a.txt`, `b.txt`, `c.txt`
4. Add some arbitrary content to the above files.
5. Add `a.txt` and `b.txt` to the staging area
6. Run git status. Understand what it says.
7. Commit `a.txt` and `b.txt` with the message "Add a.txt and b.txt"
8. Run git status. Understand what it says
9. Run git log
10. Add and Commit `c.txt`
11. Create a project on GitLab.
12. Push your code to GitLab
13. Clone your project in another directory called `git_sample_project_2`
14. In `git_sample_project_2`, add a file called `d.txt`
15. Commit and push `d.txt`
16. cd back to `git_sample_project`
17. Pull the changes from GitLab
18. Copy `git_drill_part_1.txt` to `git_sample_project`. Commit and Push it on GitLab
19. Copy `cli_drill_part_1.txt` to `git_sample_project`. Commit and Push it on GitLab
20. Add santu@mountblue.io and pramod@mountblue.io as "Developers" to your project