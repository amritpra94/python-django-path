
![little bobby tables](https://imgs.xkcd.com/comics/exploits_of_a_mom.png)

# Database Design
_____

### Overview:

* https://www.sqa.org.uk/e-learning/MDBS01CD/page_01.htm
* https://en.wikipedia.org/wiki/Database_design

_________


### Normalization:

* https://en.wikipedia.org/wiki/Database_normalization
* https://en.wikipedia.org/wiki/Unnormalized_form
* https://en.wikipedia.org/wiki/First_normal_form
* https://en.wikipedia.org/wiki/Second_normal_form
* https://en.wikipedia.org/wiki/Third_normal_form
* https://en.wikipedia.org/wiki/Boyce%E2%80%93Codd_normal_form

_________

### Blog posts:

* https://medium.com/@kimtnguyen/relational-database-schema-design-overview-70e447ff66f9
* https://snook.ca/archives/building_a_web_application/building_a_web_1

__________

### Examples:

* Examples: http://www.databaseanswers.org/data_models/

